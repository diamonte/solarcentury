 News Events API
====================
 API demo for solarcentury technical evaluation.
---------------------------------------------------

A symfony 4 project by David Sullivan.

 Intro
--------
Provides the following functionality-:
- Stores information on notable events for any year.
- Currently contains test data for years 2011, 2013 and 2018.

 How to use the service
-------------------------
First create and populate the mysql database with this command (or similar depending on mysql accounts)-:

> mysql -u root -p < news.sql

Using with the symfony web service-:

   >
   > e.g. php bin/console server:run
   > 
   > URL would be  http://localhost:8000/news/2011
   >
 
Or configure your web service (e.g. apache, nginx) as required.

 Unit Tests
-------------
 >
 > phpunit
 >
