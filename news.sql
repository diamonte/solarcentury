create database if not exists newsEvents;
grant all privileges on newsEvents.* to davids@'%' identified by 'ds';

use newsEvents;
drop table if exists news_events;

create table news_events (
	year INT,
	heading TINYTEXT,
	synopsis TEXT,
	narrative TEXT
);

insert into news_events(year, heading, synopsis, narrative) 
	values 
	( 2011,'Gaddafi killed by rebels','Former President Gaddafi of Libya was killed at hist home town of Sirte on 20th October shortly after capture by rebels. ','.....'),
	( 2011,'Earthquakes hit Northen Japan','A series of massively powerful earthquakes on 11 March 2011 unleashed a 10m tsunami sweeping away thousands of homes and leaving millions without power.','.....'),
	( 2011,'Steve Jobs dies','Steve Jobs the Apple co-founder dies from cancer 6th October 2011. ','.....'),
	( 2013,'Russia annexes Crimea','President Putin signs a bill officially annexing Crimea as part of the Russian state on March 21 2013.','.....'),
	( 2018,'Facebook data privacy scandal','Tech firm Cambridge Analytics use millions of Facebook accounts personal information in an attempt to affect US election results.','...')
	;
