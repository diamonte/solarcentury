<?php
/*
 * NewsDbTest - unit tests for NewsDb class.
 *
 */ 
namespace App;
use PHPUnit\Framework\TestCase;

class NewsDbTest extends TestCase
{
	/*
	 * Test that object constructed.
	 */
	function testValidConstruction()
	{
		$testObject = new NewsDb();
		$this->assertInstanceOf(NewsDb::class, $testObject);
	}
}
