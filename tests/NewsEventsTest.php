<?php
/*
 * NewsEventsTest - unit tests for NewsEvents class.
 */

namespace App;
use PHPUnit\Framework\TestCase;

class NewsEventsTest extends TestCase
{
	/*
	 * Tests that object constructed when using earliest valid year parameter
	 */
	function testValidConstruction()
	{
		$testObject = new NewsEvents(NewsEvents::EARLIEST_YEAR);
		$this->assertInstanceOf(NewsEvents::class, $testObject);
	}

	/*
	 * Tests that object constructed when using this year as parameter.
	 */
	function testValidConstructionCurrentYear()
	{
		$testObject = new NewsEvents(Date('Y'));
		$this->assertInstanceOf(NewsEvents::class, $testObject);
	}

	/*
	 * Tests that year before earliest handled throws exception.
	 */
	function testInvalidYearParameter()
	{
		$this->expectException(\Exception::class);
		// EARLIEST_YEAR is earliest year supported, so take the year before that.
		$ye = new NewsEvents(NewsEvents::EARLIEST_YEAR -1);
	}

	/*
	 * Tests that year in future throws exception.
	 */
	function testYearInFutureParameter()
	{
		$this->expectException(\Exception::class);
		// This year + 1 (e.g. now is 2018 so pass 2019 as year parameter)
		$ye = new NewsEvents(Date('Y') + 1);
	}

	/*
	 * Tests that exception is thrown if year parameter missing.
	 */
	function testMissingYearParameter()
	{
		$this->expectException(\Error::class);
		$ye = new NewsEvents();
	}
}
