<?php

/*
 * Base NewsEvents API class.
 * This API offers notable news events information for any valid single year input.
 * The valid years are from EARLIEST_YEAR and this year. The valid year range is 
 * arbitrary and can be extended.
 * 
 * @author David Sullivan
 */

namespace App;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Connection;

class NewsEvents
{
	const EARLIEST_YEAR = 2000;
	protected $year;
	/*
	 * @param year - defaults to this year.
	*/
	public function __construct(int $year)
	{
		$this->year = $year;

		if (!$this->isValidYear($this->year)) {
			throw new \Exception('Invalid argument:' . $year);
		}
	}

	/**
	 * getEvents()
     *
	 * @param $year - the year to retrieve notable news events for.
	 * @return dataset read from database.
	*/
	public function getEvents()
	{
		$db = new NewsDb();
		return $db->searchYear($this->year);
	}

	/**
	 * isValidYear()
     *
	 * @param $year - the year tested for validity.
	 * @return true if year is valid, false if not valid.
	*/
	protected function isValidYear(int $year) : bool
	{
		return $year >= self::EARLIEST_YEAR && $year <= Date('Y');
	}
}
