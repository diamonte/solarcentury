<?php
/*
 * NewsDb class.
 * Simple database layer for News Events API
 * Creates simple database object, with default credentials which can be overridden.
 * Provides API query of events by passing the year parameter, and returning an 
 * array of data columns containing notable news events for that year.
 */

namespace App;

class NewsDb
{
	private static $db;
	private $conn;
	const DB_CREDENTIALS = 'localhost|davids|ds|newsEvents';

	/*
	 * Construct DB object which creates connection object from DB credentials.
	 * Override default parameters by passing credentials parameter.
	 */
	public function __construct($credentials = self::DB_CREDENTIALS)
	{
		if (!is_object(self::$db[$credentials]))
		{
			list($host,$user,$pass,$dbName) = explode('|', $credentials);
			$dsn = "mysql:host=${host};dbname=${dbName}";
			self::$db[$credentials] = new \PDO($dsn, $user, $pass);
		}
		$this->conn = self::$db[$credentials];
	}

	/*
	 * Perform SELECT query on newsEvents table.
	 * Returns either an array of news event results for that year,
	 * OR null if no records are found.
	 */
	public function searchYear(int $year) :?array
	{
		$sql = "SELECT * FROM news_events WHERE year=?";
		$statement = $this->conn->prepare($sql);
		$statement->bindValue(1, $year);
		$statement->execute();
		$results = $statement->fetchAll(\PDO::FETCH_ASSOC);
		$resultsFound = array_filter($results);
		if (empty($resultsFound)) {
			return null;
		}
		return $results;
	}
}
