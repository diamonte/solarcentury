<?php

/*
 * NewsController
 *
 * Retrieves news events data from newsEvents database.

 * @api newsEvents
 * @author David Sullivan
 */

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\NewsEvents;

class NewsController extends Controller
{
	protected $year;
	// default template
	protected $template = 'noNews.html.twig';

	/*
	 * events($year)
	 *
	 * Handles a request for the year's events. 
	 * If no data for that year error page is shown.
     *
	 * @param $year - the year to fetch events for.
	 * @return Response object
	 */

	/**
	  *@Route("/news/{year}", requirements={"year"="\d{4}"})
	  */
	public function events(int $year)
	{
		// get year parameter from query string, or 
		// if parameter does not exist, use this year
		try {
			$ne = new NewsEvents($year);
		}
		catch (Exception $e) {
			return $this->render($this->template, [ 'year' => $year ]);
		}

		// get news data from database
		$results = $ne->getEvents();

		// if results is not null then output results template
		if ($results) {
			$this->template = 'news.html.twig';
		}
		return $this->render($this->template, [ 'yearsEvents' => $results, 'year' => $year ]);
	}

	/**
	  *@Route("/news/{year}", requirements={"year"="\d{1,3}"})
	  */
	public function invalidYearParameter(int $year)
	{
		return $this->render($this->template, [ 'year' => $year ]);
	}

	/**
	  *@Route("/news/")
	  */
	public function noYearParameter()
	{
		return $this->render($this->template, [ 'year' => 0 ]);
	}
}
